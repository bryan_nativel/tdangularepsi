import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LDAP_USERS } from '../model/ldap-mock-data';
import { UserLdap } from '../model/user-ldap';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  static users: UserLdap[] = LDAP_USERS;
  user: UserLdap;

  constructor() { 

  }
     
  getUsers(): Observable<UserLdap[]> {
    return of(UsersService.users);
  }
  
  getUser( login: string): Observable<UserLdap> {
    const user = UsersService.users.find(user => user.login === login)
    return of(user)
  } 
  
  
}
