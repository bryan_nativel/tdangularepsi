import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LdapDetailComponent } from '../ldap-detail/ldap-detail.component';
import { UsersService } from '../service/users.service';

@Component({
  selector: 'app-ldap-edit',
  templateUrl: '../ldap-detail/ldap-detail.component.html',
  styleUrls: ['../ldap-detail/ldap-detail.component.scss']  
})
export class LdapEditComponent extends LdapDetailComponent implements OnInit {

  constructor(usersService: UsersService,
    route: ActivatedRoute,
    fb: FormBuilder,
    router: Router) {
    super(usersService, route, fb, router);
   }

  ngOnInit(): void {
    super.onInit();

  }

  validateForm(): void {
    console.log('LdapEditComponent - validateForm');
    }
    

}
